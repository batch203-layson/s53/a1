// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';

import { useState, userContext, useContext } from "react";

import {NavLink} from "react-router-dom";

// shorthand method
import { Container, Nav, Navbar } from "react-bootstrap";


import UserContext from "../UserContext";

export default function AppNavbar() {

    // Create a user state that will be used for the conditional rendering of our nav bar
    // Use the user state
    const {user} = useContext(UserContext);

    /* 
    - "as" prop allows component to be treated as the component of the "react-router-dom" to gain access to it's properties and functionalities.
    -"to" prop is used in place of the "href" attribute for providing the URL for the page.
    */
    return (
        <Navbar bg="light" expand="lg">
            <Container fluid>
                <Navbar.Brand as={NavLink} to="/">Zuitt</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    {/*
		    	className is use instead of class, to specify a CSS class

		    	ml->ms (margin start)
		    	mr->me (margin end)
                
                if user is login, nav links visible:
                Home
                Courses
                Logout

                is user is not login, nav links visible:
                Home
                Courses
                Login
                Register
			*/}
                    <Nav className="ms-auto">
                        <Nav.Link as={NavLink} to="/" end>Home</Nav.Link>
                        <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
                        {
                            (user.electronicMail!==null)
                            ?
                                    <>
                                    <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                                    </>
                            :
                            <>
                                    <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                                    <Nav.Link as={NavLink} to="/register">Register</Nav.Link> 
                            </>
                        }
                        {/* <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                        <Nav.Link as={NavLink} to="/register">Register</Nav.Link> */}
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}
