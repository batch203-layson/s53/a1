import { useState , useEffect} from "react";

import { Card, Button } from 'react-bootstrap';

import {Link} from "react-router-dom";
// destructure the "courseProp" from the prop parameter
// CourseCard(prop)



export default function CourseCard({ courseProp }) {

    // console.log(props.courseProp.name);
    // console.log(typeof props);
    // console.log(courseProp);

    // Scenario: Keep track the number of enrollees of each course.

    // Destructure the course properties into their own variables
    //const { name, description, price } = courseProp;
    const { _id, name, description, price, slots } = courseProp;
    // Syntax:
    // const [stateName, setStateName] = useState(initialStateValue);
    // Using the state hook, it returns an array with the following elements:
    // first element contains the the current inital State value.
    // second element is a function that is used to change the value of the first element.
/* 
    const [count, setCount] = useState(0);
    const [seat, setSeat] = useState(30)
 */
    //console.log(useState(10));
/* 
    function enroll() {
        if (seat===0&&count===30)
        {
            alert("No more seats!!!!!");
        }
        else
        {
            setSeat(seat - 1);
            setCount(count + 1);
            console.log(`Enrollees: ${seat}`);
        }
    } */
   /*  const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(30) */
    /* 
        We will refactor the "enroll" function using the "useEffect" hooks to disable the enroll button when the seats reach zero
    */
    //Use the disabled state to disable the enroll button
    /* const [disabled, setDisabled] = useState(false); */

/* 
function unEnroll() {
    setCount(count-1);
} */

/* 
    function enroll()
    {
        
         if(seats>0)
        {
            setCount(count + 1);
            console.log(`Enrollees: ${count}`);
            setSeats(seats - 1);
            console.log(`Seats: ${seats}`);
        }
        else
        {
            alert("No more seats!!!!!");
        }
        
       
        setCount(count + 1);
        console.log(`Enrollees: ${count}`);
        setSeats(seats - 1);
        console.log(`Seats: ${seats}`);
        
    }

    useEffect(() => {
        if (seats<=0) 
        {
            setDisabled(true);
            alert("No more seats available.");
        }
    }, [seats])

 */
    return (
        <Card>
            <Card.Body>
                <Card.Title>
                    {name}
                </Card.Title>
                <Card.Subtitle>
                    Description:
                </Card.Subtitle>
                <Card.Text>
                    {description}
                </Card.Text>
                <Card.Subtitle>
                    Price:
                </Card.Subtitle>
                <Card.Text>
                    Php {price}
                </Card.Text>
                <Card.Subtitle>
                    Slots:
                </Card.Subtitle>
                <Card.Text>
                    {slots} available
                </Card.Text>

                <Button as={Link} to={`/courses/${_id}`} variant="primary">Details</Button>
              
            </Card.Body>
        </Card>
    )
}
