//import { Navigate } from "react-router-dom";
//import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";


import { Card, Button } from 'react-bootstrap';
// destructure the "courseProp" from the prop parameter
// CourseCard(prop)
export default function ErrorCard({ errorProp }) {

    const { name, description } = errorProp;

    const navigate = useNavigate();

    function enroll() {
        

        navigate("/");
    }

    return (
        
        <Card className="p-5 text-center">
            <Card.Body>
                <Card.Title>
                    {name}
                </Card.Title>
                <Card.Subtitle>
                    {description}
                </Card.Subtitle>
           {/*      <Card.Text>
                    
                </Card.Text>
                <Card.Subtitle>
                    Price:
                </Card.Subtitle>
                <Card.Text>
                    Php {price}
                </Card.Text>
                <Card.Text>
                    {count} enrollees
                </Card.Text> */}

                <Button variant="primary" onClick={enroll}>BACK HOME</Button>

            </Card.Body>
        </Card>

       
    )

}

/* 
onPress={() =>
          navigate('Profile', { names: ['Brent', 'Satya', 'Michaś'] })
        }
*/