const errorData = [
    {
        id: "wdc001",
        name: "404 - Not found",
        description: "The page you are looking for cannot be found.",
        //price: 45000,
        //onOffer: true
    }
]

export default errorData;