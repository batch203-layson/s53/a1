import ErrorCard from "../components/ErrorCard";

import errorData from "../data/errorData";

export default function Error() {
    

    // To confirm if we have import our mock database.
    console.log(errorData);

    console.log(errorData[0]);
    // The curly braces ({}) are used for props to signify that we are providing information using JavaScript expressions.

    // Everytime the map method loops through the data, it creates "CourseCard" component and then passes the current element in our coursesDara array using courseProp.
    // Multiple components created through the map method maust have a unique key that will help ReactJS identify which components/elements have been changed, added, or removed.
    const errors = errorData.map(error => {
        return (
            <ErrorCard key={error.id} errorProp={error} />
        );
    });


    return (
        <>
            
            {errors}
        </>
    )


}